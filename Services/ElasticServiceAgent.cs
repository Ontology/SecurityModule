﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SecurityModule.Services
{
    public class ElasticServiceAgent : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        

        private OntologyModDBConnector dbReaderOItems;
        private OntologyModDBConnector dbReaderMasterPassword;
        private OntologyModDBConnector dbReaderPasswords;
        private OntologyModDBConnector dbReaderRefUsers;
        private OntologyModDBConnector dbReaderRefPasswords;
        private OntologyModDBConnector dbReaderUserToPassword;

        private Thread getSecurityItemsAsync;

        private clsOntologyItem oItemRef;

        private clsOntologyItem resultSecurityItems;
        public clsOntologyItem ResultSecurityItems
        {
            get { return resultSecurityItems; }
            set
            {
                resultSecurityItems = value;
                RaisePropertyChanged(NotifyChanges.NotifyChanges.Elastic_ResultSecurityItems);
            }
        }

        public List<clsObjectRel> RefToUsers
        {
            get
            {
                return dbReaderRefUsers.ObjectRels;
            }
        }

        public List<clsObjectRel> RefPasswords
        {
            get
            {
                return dbReaderRefPasswords.ObjectRels;
            }
        }

        public List<clsObjectRel> UsersToPasswords
        {
            get
            {
                return dbReaderUserToPassword.ObjectRels;
            }
        }

        public ElasticServiceAgent(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        public clsOntologyItem GetUser(string userName)
        {
            return GetSecItem(userName, localConfig.OItem_type_user);
        }

        public clsOntologyItem GetGroup(string groupName)
        {
            return GetSecItem(groupName, localConfig.OItem_type_group);
        }

        public clsOntologyItem GetPasswordOfUser(clsOntologyItem oItemUser)
        {
            var searchPassword = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemUser.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_secured_by.GUID
                }
            };

            var result = dbReaderPasswords.GetDataObjectRel(searchPassword);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var passwordItem = dbReaderPasswords.ObjectRels.Select(password => new clsOntologyItem
                {
                    GUID = password.ID_Other,
                    Name = password.Name_Other,
                    GUID_Parent = password.ID_Parent_Other,
                    Type = password.Ontology
                }).FirstOrDefault();

                return passwordItem;
            }

            return null;
        }

        private clsOntologyItem GetSecItem(string name, clsOntologyItem secItemClass)
        {
            var searchSecItems = new List<clsOntologyItem> { new clsOntologyItem
                {
                    Name = name,
                    GUID_Parent = secItemClass.GUID
                }
            };

            var result = dbReaderOItems.GetDataObjects(searchSecItems);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return null;
            }

            return dbReaderOItems.Objects1.FirstOrDefault(secItem => secItem.Name.ToLower() == name.ToLower());
        }

        public clsOntologyItem GetSecuredItems(clsOntologyItem oItemRef)
        {
            StopRead();

            this.oItemRef = oItemRef;
            var result = localConfig.Globals.LState_Success.Clone();

            getSecurityItemsAsync = new Thread(GetSecuredItemsAsync);
            getSecurityItemsAsync.Start();

            return result;
        }

        private void GetSecuredItemsAsync()
        {

            var result = Get_001_RefUsers();

            if (result.GUID == localConfig.Globals.LState_Error.GUID) ResultSecurityItems = result;

            result = Get_002_RefPasswords();

            if (result.GUID == localConfig.Globals.LState_Error.GUID) ResultSecurityItems = result;

            result = Get_003_UsersToPassword();

            ResultSecurityItems = result;


        }

        private clsOntologyItem Get_001_RefUsers()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var searchUsers = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemRef.GUID,
                    ID_Parent_Other = localConfig.OItem_type_user.GUID
                }
            };


            result = dbReaderRefUsers.GetDataObjectRel(searchUsers);

            return result;
        }

        private clsOntologyItem Get_002_RefPasswords()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var searchPasswords = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_RelationType = localConfig.OItem_relationtype_secured_by.GUID
                }
            };

            result = dbReaderRefPasswords.GetDataObjectRel(searchPasswords);

            return result;
        }

        private clsOntologyItem Get_003_UsersToPassword()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var searchUserToPassword = dbReaderRefUsers.ObjectRels.Select(refUser => new clsObjectRel
            {
                ID_Object = refUser.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_secured_by.GUID
            }).ToList();

            result = dbReaderUserToPassword.GetDataObjectRel(searchUserToPassword);

            return result;

        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            return dbReaderOItems.GetOItem(id, type);
        }

        public clsOntologyItem GetEcryptedMasterPassword(string idUser)
        {
            var searchMasterPassword = new List<clsObjectAtt>
            {
                new clsObjectAtt
                {
                    ID_Object = idUser,
                    ID_AttributeType = localConfig.OItem_attribute_master_password.GUID
                }
            };

            var result = dbReaderMasterPassword.GetDataObjectAtt(searchMasterPassword);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            if (!dbReaderMasterPassword.ObjAtts.Any())
            {
                return localConfig.Globals.LState_Nothing.Clone();
            }

            result.Additional1 = dbReaderMasterPassword.ObjAtts.First().Val_String;
            return result;

        }

        public void StopRead()
        {
            if (getSecurityItemsAsync != null)
            {
                try
                {
                    getSecurityItemsAsync.Abort();
                }
                catch(Exception ex) { }
            }
            
        }

        private void Initialize()
        {
            dbReaderOItems = new OntologyModDBConnector(localConfig.Globals);
            dbReaderMasterPassword = new OntologyModDBConnector(localConfig.Globals);
            dbReaderPasswords = new OntologyModDBConnector(localConfig.Globals);
            dbReaderRefUsers = new OntologyModDBConnector(localConfig.Globals);
            dbReaderRefPasswords = new OntologyModDBConnector(localConfig.Globals);
            dbReaderUserToPassword = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
