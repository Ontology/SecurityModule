﻿using OntoMsg_Module.Attributes;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule.Models
{
    public class PasswordItem
    {
        [DataViewColumn(Caption = "Id", CellType = CellType.String, IsVisible = false, IsIdField = true)]
        public string Id { get; set; }

        [DataViewColumn(Caption = "Password", CellType = CellType.String, IsVisible = true, DisplayOrder = 2)]
        public string PasswordMask { get; set; }

        [DataViewColumn(Caption = "IdRef", CellType = CellType.String, IsVisible = false)]
        public string IdRef { get; set; }

        [DataViewColumn(Caption = "Reference", CellType = CellType.String, IsVisible = true, DisplayOrder = 0)]
        public string NameRef { get; set; }

        [DataViewColumn(Caption = "Reference-Parent", CellType = CellType.String, IsVisible = true, DisplayOrder = 1)]
        public string ParentRef { get; set; }

        [DataViewColumn(Caption = "IdUser", CellType = CellType.String, IsVisible = false)]
        public string IdUser { get; set; }

        [DataViewColumn(Caption = "NameUser", CellType = CellType.String, IsVisible = true)]
        public string NameUser { get; set; }
    }
}
