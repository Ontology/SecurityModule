﻿using OntologyClasses.BaseClasses;
using SecurityModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule.Factories
{
    public class SecItemsFactory
    {

        private clsLocalConfig localConfig;

        public List<PasswordItem> GetSecurityItems(List<clsObjectRel> refToUsers,
            List<clsObjectRel> userToPasswords,
            List<clsObjectRel> refToPasswords,
            clsOntologyItem refItem)
        {
            var secList = (from refToUser in refToUsers
                           join userToPassword in userToPasswords on refToUser.ID_Other equals userToPassword.ID_Object
                           select new PasswordItem
                           {
                               Id = userToPassword.ID_Other,
                               IdRef = refToUser.ID_Object,
                               NameRef = refToUser.Name_Object,
                               IdUser = refToUser.ID_Other,
                               NameUser = refToUser.Name_Other,
                               ParentRef = refToUser.Name_Parent_Object,
                               PasswordMask = "*****"
                           }).ToList();

            secList.AddRange(refToPasswords.Where(refToPassword => refToPassword.ID_Object == refItem.GUID).Select(refToPassword => new PasswordItem
            {
                Id = refToPassword.ID_Other,
                IdRef = refToPassword.ID_Object,
                NameRef = refToPassword.Name_Object,
                PasswordMask = "*****"
            }));

            secList.AddRange(refToPasswords.Where(refToPassword => refToPassword.ID_Other == refItem.GUID).Select(refToPassword => new PasswordItem
            {
                Id = refToPassword.ID_Object,
                IdRef = refToPassword.ID_Other,
                NameRef = refToPassword.Name_Other,
                PasswordMask = "*****"
            }));

            return secList;
        }

        public SecItemsFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }

    }
}
