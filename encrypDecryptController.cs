﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace SecurityModule
{
    internal class EncrypDecryptController
    {
        public string EncodeString(string password, string key)
        {
            var rd = new RijndaelManaged();
            var md5 = new MD5CryptoServiceProvider();
            var byteKey = md5.ComputeHash(Encoding.UTF8.GetBytes(key));

            md5.Clear();

            rd.Key = byteKey;
            rd.GenerateIV();

            var iv = rd.IV;
            byte[] encdata;
            using (var ms = new MemoryStream())
            {
                ms.Write(iv, 0, iv.Length);
                using (var cs = new CryptoStream(ms, rd.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    var data = Encoding.UTF8.GetBytes(password);

                    cs.Write(data, 0, data.Length);
                    cs.FlushFinalBlock();
                    encdata = ms.ToArray();
                }
            }

            return Convert.ToBase64String(encdata);
        }

        public string DecodeString(string password, string key)
        {
            var rd = new RijndaelManaged();
            var rijandaelIvLength = 16;
            var md5 = new MD5CryptoServiceProvider();
            var byteKey = md5.ComputeHash(Encoding.UTF8.GetBytes(key));

            md5.Clear();

            var encdata = Convert.FromBase64String(password);

            byte[] data;
            int length;

            using (var ms = new MemoryStream(encdata))
            {
                var iv = new byte[16];

                ms.Read(iv, 0, rijandaelIvLength);

                rd.IV = iv;
                rd.Key = byteKey;

                
                using (var cs = new CryptoStream(ms, rd.CreateDecryptor(), CryptoStreamMode.Read))
                {
                    data = new Byte[ms.Length - rijandaelIvLength];
                    length = cs.Read(data, 0, data.Length);
                }
            }

            return Encoding.UTF8.GetString(data, 0, length);
        }
    }
}
