﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using SecurityModule.Factories;
using SecurityModule.Models;
using SecurityModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule
{
    public delegate void LoadedSecurityItems(List<PasswordItem> passwordItems);
    public class SecurityController
    {
        private clsLocalConfig localConfig;

        private ElasticServiceAgent esAgent;
        private EncrypDecryptController encryptDecryptController = new EncrypDecryptController();
        private string masterPasswordDecrypted;

        public event LoadedSecurityItems loadedSecurityItems;

        private SecItemsFactory secItemsFactory;

        private clsOntologyItem refItem;

        public bool IsInitialized { get; private set; }

        public SecurityController()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        public SecurityController(Globals globals)
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        public clsOntologyItem GetSecurityItems(clsOntologyItem oItemRef)
        {
            refItem = oItemRef;
            var result = localConfig.Globals.LState_Success.Clone();

            return esAgent.GetSecuredItems(oItemRef);
        }

        public clsOntologyItem GetUser(string userName)
        {
            return esAgent.GetUser(userName);
        }

        public clsOntologyItem GetGroup(string groupName)
        {
            return esAgent.GetGroup(groupName);
        }

        public string DecodePassword(clsOntologyItem oItemSec)
        {
            if (!IsInitialized) return null;

            

            if (oItemSec.GUID_Parent == localConfig.OItem_type_user.GUID)
            {
                var passwordItem = esAgent.GetPasswordOfUser(oItemSec);

                if (passwordItem == null) return null;

                string decodedPassword = encryptDecryptController.DecodeString(passwordItem.Name, masterPasswordDecrypted);

                return decodedPassword;
            }
            else
            {
                string decodedPassword = encryptDecryptController.DecodeString(oItemSec.Name, masterPasswordDecrypted);

                return decodedPassword;
            }
            


        }

        public string DecodePassword(PasswordItem passwordItem)
        {
            if (!IsInitialized) return null;

            var encryptedPassword = esAgent.GetOItem(passwordItem.Id, localConfig.Globals.Type_Object);

            if (encryptedPassword == null) return null;

            string decodedPassword = encryptDecryptController.DecodeString(encryptedPassword.Name, masterPasswordDecrypted);

            return decodedPassword;

        }

        public clsOntologyItem InitializeUser(clsOntologyItem oItemUser, string password)
        {
            if (!IsInitialized)
            {
                var result = esAgent.GetEcryptedMasterPassword(oItemUser.GUID);

                if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

                masterPasswordDecrypted = encryptDecryptController.DecodeString(result.Additional1, password);

                if (masterPasswordDecrypted == password)
                {
                    IsInitialized = true;
                    return localConfig.Globals.LState_Success.Clone();
                    
                }
                else
                {
                    IsInitialized = false;
                    return localConfig.Globals.LState_Error.Clone();
                }
            }
            else
            {
                IsInitialized = false;
                return localConfig.Globals.LState_Success.Clone();
            }
        }

        private void Initialize()
        {
            esAgent = new ElasticServiceAgent(localConfig);
            esAgent.PropertyChanged += EsAgent_PropertyChanged;

            secItemsFactory = new SecItemsFactory(localConfig);
        }

        private void EsAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == NotifyChanges.NotifyChanges.Elastic_ResultSecurityItems)
            {
                if (esAgent.ResultSecurityItems.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    if (loadedSecurityItems != null)
                    {
                        loadedSecurityItems(secItemsFactory.GetSecurityItems(esAgent.RefToUsers,
                            esAgent.UsersToPasswords,
                            esAgent.RefPasswords,
                            refItem));
                    }
                }
                else
                {
                    if (loadedSecurityItems != null)
                    {
                        loadedSecurityItems(null);
                    }
                }
            }
        }
    }
}
